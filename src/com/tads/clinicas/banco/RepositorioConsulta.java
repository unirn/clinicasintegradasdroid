package com.tads.clinicas.banco;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tads.clinicas.modelo.Consulta;

public class RepositorioConsulta {
	private final String NOME_BANCO = "clinicas.db";
	private final String NOME_TABELA = "consultas";
	private SQLiteDatabase db;
	
	public RepositorioConsulta(Context ctx){
		db = ctx.openOrCreateDatabase(NOME_BANCO, Context.MODE_PRIVATE, null);
	}
	
	public void close(){
		db.close();
	}
	
	public int salvarLista(List<Consulta> consultas){
		int qtd = 0;
		
		for (Consulta consulta : consultas) {
			consulta.setId(0);
			salvar(consulta);
			qtd++;
		}
		
		return qtd;
	}
	
	public int salvar(Consulta consulta){
		int id = consulta.getId();
		
		if(id != 0)
			atualizar(consulta);
		else
			id = inserir(consulta);
		
		return id;
	}
	
	private int inserir(Consulta consulta) {
		ContentValues values = new ContentValues();
		
		values.put(Consulta.ID_CONSULTA, consulta.getId_consulta());
		values.put(Consulta.PACIENTE, consulta.getPaciente());
		values.put(Consulta.DATA_CONSUTA, consulta.getDataConsulta());
		values.put(Consulta.HORARIO_CONSULTA, consulta.getHoraConsulta());
		values.put(Consulta.CLINICA, consulta.getClinica());
		values.put(Consulta.STATUS_CONSULTA, consulta.getStatusConsulta());
		
		int id = (int) inserir(values);
		return id;
	}
	
	private long  inserir(ContentValues values) {
		long id = db.insert(NOME_TABELA, null, values);
		return id;
	}

	private int atualizar(Consulta consulta) {
		ContentValues values = new ContentValues();
		
		String _id = String.valueOf(consulta.getId());
		
		values.put(Consulta.ID_CONSULTA, consulta.getId_consulta());
		values.put(Consulta.PACIENTE, consulta.getPaciente());
		values.put(Consulta.DATA_CONSUTA, consulta.getDataConsulta());
		values.put(Consulta.HORARIO_CONSULTA, consulta.getHoraConsulta());
		values.put(Consulta.CLINICA, consulta.getClinica());
		values.put(Consulta.STATUS_CONSULTA, consulta.getStatusConsulta());
		
		String where = Consulta.ID + "=?";
		String[] whereArgs = new String[] { _id };
		
		int count = atualiza(values, where, whereArgs);
		
		return count;
	}

	private int atualiza(ContentValues values, String where, String[] whereArgs) {
		int count = db.update(NOME_TABELA, values, where, whereArgs);
		Log.i("TESTE ATUALIZACAO", count + " registros");
		
		return count;
	}
	
	public Cursor getCursor(){
		return db.query(NOME_TABELA, null, null, null, null, null, null);
	}
	
	public List<Consulta> listarConsultas(){
		Cursor cursor = getCursor();
		
		List<Consulta> listaConsulta = new ArrayList<Consulta>();
		
		if(cursor.moveToFirst()){	
			while(!cursor.isAfterLast()) {
				Consulta consulta = new Consulta();
				consulta.setId(cursor.getInt(0));
				consulta.setId_consulta(cursor.getInt(1));
				consulta.setPaciente(cursor.getString(2));
				consulta.setDataConsulta(cursor.getString(3));
				consulta.setHoraConsulta(cursor.getString(4));
				consulta.setClinica(cursor.getString(5));
				consulta.setStatusConsulta(cursor.getString(6));

				listaConsulta.add(consulta);
				
			    cursor.moveToNext();
			}
		}
		
		return listaConsulta;
	}
	
	public int deletarTodos(){
		int qtd = db.delete(NOME_TABELA, null, null);
		
		return qtd;
	}
}