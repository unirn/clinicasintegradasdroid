package com.tads.clinicas;

import com.tads.clinicas.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ImageButton btPaciente = (ImageButton) findViewById(R.id.btPaciente);
        btPaciente.setOnClickListener(btPacienteOnClickListener);
        
        ImageButton btHistorico = (ImageButton) findViewById(R.id.btHistorico);
        btHistorico.setOnClickListener(btHistoricoOnClickListener);
    }
    
    
    private OnClickListener btPacienteOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
        	Intent it = new Intent(MainActivity.this, BuscarConsultaCpfActivity.class);
			startActivity(it);
        }
    };
    
    private OnClickListener btHistoricoOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
        	Intent it = new Intent(MainActivity.this, ListarHistoricoActivity.class);
			startActivity(it);
        }
    };
}