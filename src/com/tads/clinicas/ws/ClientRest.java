package com.tads.clinicas.ws;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.tads.clinicas.modelo.Consulta;

public class ClientRest {
	private String ip = "10.17.1.137:8080";
	private String URL_BUSCAR_POR_CPF = "http://" + ip + "/recepcao/service/consultaPaciente/?cpf=";
	
	public Consulta getConsulta(int cpf) throws Exception {
		String[] resposta = new WebServiceClient().get(URL_BUSCAR_POR_CPF + cpf);

		if (resposta[0].equals("200")) {
			Gson gson = new Gson();
			Consulta consulta = new Consulta();
			consulta = gson.fromJson(resposta[1], Consulta.class);
			return consulta;
		} else {
			throw new Exception(resposta[1]);
		}
	}
	
	public List<Consulta> getListaConsulta(String cpf) throws Exception {
		String[] resposta = new WebServiceClient().get(URL_BUSCAR_POR_CPF + cpf);
		List<Consulta> listaConsultas = new ArrayList<Consulta>();

		if (resposta[0].equals("200")) {
			Gson gson = new Gson();
			
			JsonParser parser = new JsonParser();
			JsonArray array = parser.parse(resposta[1]).getAsJsonArray();
			
			for (JsonElement jsonElement : array) {
				listaConsultas.add(gson.fromJson(jsonElement, Consulta.class));
			}
			
			return listaConsultas;
		} else {
			throw new Exception(resposta[1]);
		}
	}
	
	public String confirmarConsulta(int idConsulta, int idStatus) throws Exception {
		String url = "http://" + ip + "/recepcao/service/alterarStatusConsulta/?idconsulta="+ idConsulta + "&idstatus=" + idStatus;
		String[] resposta = new WebServiceClient().get(url);

		if (resposta[0].equals("200")) {
			return resposta[1];
		} else {
			throw new Exception(resposta[1]);
		}
	}
}