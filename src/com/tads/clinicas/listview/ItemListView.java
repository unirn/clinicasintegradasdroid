package com.tads.clinicas.listview;

public class ItemListView {
	private int tipoClinica;
	private String textoClinica;
    private String textoData;
    private String textoStatus;
    
    public ItemListView(int tipoClinica, String textoClinica, String textoData, String textoStatus) {
    	this.tipoClinica = tipoClinica;
    	this.textoClinica = textoClinica;
    	this.textoData = textoData;
    	this.textoStatus = textoStatus;
    }

	public int getTipoClinica() {
		return tipoClinica;
	}

	public void setTipoClinica(int tipoClinica) {
		this.tipoClinica = tipoClinica;
	}

	public String getTextoClinica() {
		return textoClinica;
	}

	public void setTextoClinica(String textoClinica) {
		this.textoClinica = textoClinica;
	}

	public String getTextoData() {
		return textoData;
	}

	public void setTextoData(String textoData) {
		this.textoData = textoData;
	}

	public String getTextoStatus() {
		return textoStatus;
	}

	public void setTextoStatus(String textoStatus) {
		this.textoStatus = textoStatus;
	}
}