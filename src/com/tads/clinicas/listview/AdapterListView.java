package com.tads.clinicas.listview;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ImageView;

import com.tads.clinicas.R;

public class AdapterListView extends BaseAdapter {
	private LayoutInflater mInflater;
	private ArrayList<ItemListView> itens;

	
	public AdapterListView(Context context, ArrayList<ItemListView> itens) {
		this.itens = itens;
		mInflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return itens.size();
	}

	public ItemListView getItem(int position) {
		return itens.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View view, ViewGroup parent) {
		ItemListView item = itens.get(position);
		view = mInflater.inflate(R.layout.item_listview, null);

		if (item.getTipoClinica() == 1)
			((ImageView) view.findViewById(R.id.iconeLista)).setImageResource(R.drawable.ico_psicologia);
		
		else if (item.getTipoClinica() == 2)
			
			((ImageView) view.findViewById(R.id.iconeLista)).setImageResource(R.drawable.ico_fisioterapia);
		else
			((ImageView) view.findViewById(R.id.iconeLista)).setImageResource(R.drawable.ajuda);
		

		((TextView) view.findViewById(R.id.textClinica)).setText(item.getTextoClinica());
		((TextView) view.findViewById(R.id.textData)).setText(item.getTextoData());
		((TextView) view.findViewById(R.id.textStatus)).setText(item.getTextoStatus());

		return view;
	}
}