package com.tads.clinicas;

import com.tads.clinicas.modelo.Consulta;
import com.tads.clinicas.ws.ClientRest;
import com.tads.clinicas.R;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;

public class BuscarConsultaCpfActivity extends Activity {
	private TextView exibirNome,
				exibirData,
				exibirHora,
				exibirClinica,
				exibirStatus;
	private LinearLayout layoutInformacoes,
				layoutBusca;
	private Button btBuscar,
				btEditar;
	private EditText cpfBusca;
	private RadioGroup tipoStatus;
	private CheckBox isCheckSalvarConsulta;
	private Bundle paramConsulta;
	private int idAtual = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.buscar_por_cpf);

		cpfBusca = (EditText) findViewById(R.id.buscarCpf);
		btBuscar = (Button) findViewById(R.id.btBuscar);
		isCheckSalvarConsulta = (CheckBox) findViewById(R.id.checkBoxSalvar);

		carregarCampos();

		Intent intent = getIntent();

		// verifica se esta recebendo parametros de outra tala,
		// no caso para exibir detalhes da consulta selecionada
		paramConsulta = intent.getExtras();
		if (paramConsulta != null) {
			Consulta consultaAtual = montarObjetoConsulta(paramConsulta);
			preencherCampos(consultaAtual);
		}

		btBuscar.setOnClickListener(btBuscarOnClickListener);
		btEditar.setOnClickListener(btEditarOnClickListener);
	}

	private OnClickListener btBuscarOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (!cpfBusca.getText().toString().equals("")) {
				Intent it = new Intent(BuscarConsultaCpfActivity.this,
						ListarActivity.class);
				Bundle parametros = new Bundle();
				parametros.putString("cpf", cpfBusca.getText().toString());
				it.putExtras(parametros);
				startActivity(it);
			} else {
				Toast.makeText(getApplicationContext(), "Digite seu CPF",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	private OnClickListener btEditarOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			int status = -1;

			switch (tipoStatus.getCheckedRadioButtonId()) {
			case R.id.statusConfirmado:
				status = 2;
				Toast.makeText(BuscarConsultaCpfActivity.this,
						"Consulta confirmada.", Toast.LENGTH_LONG).show();

				break;
			case R.id.statusCancelado:
				status = 4;
				Toast.makeText(BuscarConsultaCpfActivity.this,
						"Consulta cancelada.", Toast.LENGTH_LONG).show();
				break;
			}

			try {
				ClientRest clienteRest = new ClientRest();
				clienteRest.confirmarConsulta(idAtual, status);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				Intent intent = new Intent(BuscarConsultaCpfActivity.this,
						MainActivity.class);
				startActivity(intent);
			}
		}
	};

	private void carregarCampos() {
		layoutInformacoes = (LinearLayout) findViewById(R.id.layoutInformacoes);
		layoutBusca = (LinearLayout) findViewById(R.id.layoutBusca);
		exibirNome = (TextView) findViewById(R.id.exibiNome);
		exibirData = (TextView) findViewById(R.id.exibirData);
		exibirHora = (TextView) findViewById(R.id.exibirHora);
		exibirClinica = (TextView) findViewById(R.id.exibirClinica);
		exibirStatus = (TextView) findViewById(R.id.exibirStatus);
		tipoStatus = (RadioGroup) findViewById(R.id.tipoStatus);
		btEditar = (Button) findViewById(R.id.btEdita);
	}

	private Consulta montarObjetoConsulta(Bundle param) {
		Consulta consulta = new Consulta();

		consulta.setId(param.getInt(Consulta.ID)); // id da consulta no banco local
		consulta.setId_consulta(param.getInt(Consulta.ID_CONSULTA)); // id da consulta no servidor
		consulta.setPaciente(param.getString(Consulta.PACIENTE));
		consulta.setDataConsulta(param.getString(Consulta.DATA_CONSUTA)); // formatar data
		consulta.setHoraConsulta(param.getString(Consulta.HORARIO_CONSULTA));
		consulta.setClinica(param.getString(Consulta.CLINICA));
		consulta.setStatusConsulta(param.getString(Consulta.STATUS_CONSULTA));

		return consulta;
	}

	private void preencherCampos(Consulta c) {
		layoutInformacoes.setVisibility(View.VISIBLE);
		layoutBusca.setVisibility(View.GONE);
		isCheckSalvarConsulta.setVisibility(View.VISIBLE);

		idAtual = c.getId();

		exibirNome.setText(c.getPaciente());
		exibirData.setText(c.getDataConsulta());
		exibirHora.setText(c.getHoraConsulta());
		exibirClinica.setText(c.getClinica());
		exibirStatus.setText(c.getStatusConsulta());
	}
}